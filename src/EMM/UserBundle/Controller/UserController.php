<?php

namespace EMM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormError;
use EMM\UserBundle\Entity\User;
use EMM\UserBundle\Form\UserType;


class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        //$users = $em->getRepository('EMMUserBundle:User')->findAll();
        
        /*
        $res = 'Lista de Usuarios: <br/>';
        
        foreach($users as $user)
        {
            $res .= 'Usuario: ' . $user->getUsername() . ' - Email: ' . $user->getEmail() . '<br/>';
            
            
        }
        
            return new Response($res);
            
            */
            
            //creando la consulta nativa
            $dql = "SELECT u FROM EMMUserBundle:User u ORDER BY u.id DESC"; //u es un alias
            
            //ejecutando la consulta
            $users = $em->createQuery($dql);
            
            //creamos una variable paginator para llamar al paginator
            $paginator = $this->get('knp_paginator');
            
            $pagination = $paginator->paginate(
                
                //parametro inicial es la consulta ejecutada
                $users, 
                //para que el paginator se situe en la pagina 1
                $request->query->getInt('page', 1),
                //configurando el limite por pagina
                10
                );
            
            
            return $this->render('EMMUserBundle:User:index.html.twig', array('pagination'=> $pagination));
    }
    
    public function addAction()
    {
        
        //creamos una instancia llamada user
        $user = new User();
        //creamos el form y le enviamos la entidad user
        $form = $this->createCreateForm($user);
        
        //renderizamos el formulario
        return $this->render('EMMUserBundle:User:add.html.twig', array('form' => $form->createView()));
        
    }
    
    
    
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('emm_user_create'),
            'method' => 'POST'
            
            ));
            
            return $form;
    }
    
    public function createAction(Request $request)
    {
        
        //creamos una variable llamada user
        $user = new User();
        //creamos el form y le enviamos la entidad user
        $form = $this->createCreateForm($user);
        //obteniendo la variable request
        $form->handleRequest($request);
        
        if ($form->isValid())
        {
            //encriptando el password
            //recuperando el password sin encriptar que viene del form
            $password = $form->get('password')->getData();
            //validando que el password no este vacio
            $passwordConstraint = new Assert\NotBlank();
            //configurando el error
            $errorList = $this->get('validator')->validate($password, $passwordConstraint);

            if (count($errorList) == 0)
            {
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user,$password);
                $user->setPassword($encoded);
            
                //cargamos el objeto manager
                $em = $this->getDoctrine()->getManager();
                //utilizamos el metodo persist pasandole el user
                $em->persist($user);
                //cargar cada uno de los campos en la base de datos
                $em->flush();
                
                $successMessage = $this->get('translator')->trans('The user has been created.');
                $this->addFlash('mensaje', $successMessage);
                
                //redireccionamos a la ruta
                return $this->redirectToRoute('emm_user_index');
            }
            //si existe algun error
            else
            {
                $errorMessage = new FormError($errorList[0]->getMessage());
                $form->get('password')->addError($errorMessage);
            }
            
        }
        
        //renderizamos el formulario si hay un problema
        return $this->render('EMMUserBundle:User:add.html.twig', array('form' => $form->createView()));
        
    }
    
    public function editAction($id)
    {
        //recuperamos el manager
        $em = $this->getDoctrine()->getManager();
        
        //recuperamos el registro del usuario que vamos a editar
        $user = $em->getRepository('EMMUserBundle:User')->find($id);
        
        //arrojamos una excepcion si no se encuentra ningun usuario
        if (!$user)
        {
            //creando la variable con el mensaje para manejar traducciones.
            
            $messageException = $this->get('translator')->trans('User not found.');
            
            //arrojando la excepcion con el mensaje traducido.
            
            throw $this->createNotFoundException($messageException);
        }
        
        //creando el formulario
        
        $form = $this->createEditForm($user); 
        
        //renderizamos el formulario
        
        return $this->render('EMMUserBundle:User:edit.html.twig', array('user'=>$user, 'form'=>$form->createView() ));
        
    }
    
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array('action'=>$this->generateUrl('emm_user_update', array('id'=>$entity->getId())), 'method'=>'PUT'));
        
        return $form;
    }
    
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('EMMUserBundle:User')->find($id);
        
        //arrojamos una excepcion si no se encuentra ningun usuario
        if (!$user)
        {
            //creando la variable con el mensaje para manejar traducciones.
            
            $messageException = $this->get('translator')->trans('User not found.');
            
            //arrojando la excepcion con el mensaje traducido.
            
            throw $this->createNotFoundException($messageException);
        }
        
        $form = $this->createEditForm($user);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            //recuperando el password colocado por el usuario en update
            
            $password = $form->get('password')->getData();
            
            //encriptando el nuevo password si no esta vacio
            if(!empty($password))
            {
               
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user,$password);
                $user->setPassword($encoded);
                
            }
            else
            {
                $recoverPass = $this->recoverPass($id);
                //para ver rapido la data en $recoverPass
                //print_r($recoverPass);
                //exit();
                
                $user->setPassword($recoverPass[0]['password']);
                
            }
            
            //haciendo que el usuario tipo admin siempre este activo aunque se desactive la casilla al actualizar
            
            if($form->get('role')->getData() == 'ROLE_ADMIN')
            {
                $user->setIsActive(1);
            }
            
            $em->flush();
            $successMessage = $this->get('translator')->trans('The user has been modified.');
            
            $this->addFlash('mensaje', $successMessage);
            
            return $this->redirectToRoute('emm_user_edit', array('id' => $user->getId()));
        }
        
        return $this->render('EMMUserBundle:User:edit.html.twig',array('user' => $user, 'form' => $form->createView()));
        
        
    }// fin de la accion de actualizar
    
    private function recoverPass($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQuery(
            
        'SELECT u.password
         FROM EMMUserBundle:User u
         WHERE u.id = :id' 
        )->setParameter('id', $id);
        
        $currentPass = $query->getResult();
        
        return $currentPass;
    }
    
    
    public function viewAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('EMMUserBundle:User');
        
        $user = $repository->find($id);
        
        //validando si existe el usuario
        if (!$user)
        {
            //creando la variable con el mensaje para manejar traducciones.
            
            $messageException = $this->get('translator')->trans('User not found.');
            
            //arrojando la excepcion con el mensaje traducido.
            
            throw $this->createNotFoundException($messageException);
        }
        
        //creando el form del delete
        
        $deleteForm = $this->createDeleteForm($user);
        
        return $this->render('EMMUserBundle:User:view.html.twig', array('user' => $user, 'delete_form' => $deleteForm->createView()));
    }
    
    private function createDeleteForm($user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('emm_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
    
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('EMMUserBundle:User')->find($id);
        
        //validando si existe el usuario
        if (!$user)
        {
            //creando la variable con el mensaje para manejar traducciones.
            
            $messageException = $this->get('translator')->trans('User not found.');
            
            //arrojando la excepcion con el mensaje traducido.
            
            throw $this->createNotFoundException($messageException);
        }
        
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            //eliminando el registro
            $em->remove($user);
            //aplicando esto en la base de datos
            $em->flush();
            
            $successMessage = $this->get('translator')->trans('The user has been deleted.');
            $this->addFlash('mensaje', $successMessage);
            
            //redireccionamos a la ruta
            return $this->redirectToRoute('emm_user_index');
            
        }
        
    }
    
    
     public function articlesAction($page)
    {
        return new Response('Este es mi articulo' . ' ' . $page );
    }
}
